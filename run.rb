require "csv"
require "sequel"
require "mysql2"
require "builder"

require_relative "lib/cutlers.rb"
require_relative "lib/edinburgh.rb"
require_relative "lib/freedom.rb"
require_relative "lib/harcourts.rb"
require_relative "lib/livingcorporation.rb"
require_relative "lib/ljhooker.rb"
require_relative "lib/metro.rb"
require_relative "lib/nidd.rb"
require_relative "lib/pgg.rb"
require_relative "lib/raywhite.rb"
require_relative "lib/realestate.rb"
require_relative "lib/southernwide.rb"
require_relative "lib/trademe.rb"

class PropertyScraper
  PRICE_MAX = 300_000

  DB = Sequel.connect(
    :adapter => "mysql2",
    :host => "127.0.0.1",
    :username => "root",
    :password => "",
    :database => "property_scrape"
  )

  SITES = [
    Cutlers,
    Edinburgh,
    Freedom,
    Harcourts,
    Ljhooker,
    Livingcorporation,
    Metro,
    Nidd,
    Pgg,
    Raywhite,
    Realestate,
    Southernwide,
    Trademe,
  ]

  def run
    puts "Starting #{Time.now}"

    SITES.each do |site|
      puts site
      new_properties = begin
        process_data(site.new.farm)
      rescue Exception => e
        puts site
        puts e
        []
      end
      unless new_properties.empty?
        puts new_properties
      else
        puts "#{site} - No updates"
      end
    end

    output_html
  end

  def process_data(data)
    new_properties = []
    data.flatten.each do |property|
      if DB[:houses].where(link: property[:link]).count == 0
        new_properties << property[:address]
        DB[:houses].insert(property.merge({created_at: Time.now}))
      end
    end

    # binding.pry
    # CSV.open("listings.csv", "w", headers: data.flatten.first.keys, :write_headers => true) do |csv|
    #   data.flatten.each do |h|
    #     csv << h.values
    #   end
    # end

    new_properties
  end

  def output_html
    data = DB[:houses].where{created_at > Date.today - 14}.order(Sequel.desc(:id)).to_a
    html = '<head><script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.7.1/clipboard.min.js"></script>'
    html << "<!-- Global site tag (gtag.js) - Google Analytics --><script async src='https://www.googletagmanager.com/gtag/js?id=UA-114179857-1'></script><script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'UA-114179857-1');</script>"
    html << "<style>table{border-collapse: collapse;}td{border: 1px solid grey;} .btn img{width:20px;height:20px;}</style>"
    html << "<br>Updated at #{Time.now}. <br />"
    html << <<-headers
      <tr>
        <td>Address</td>
        <td>Price</td>
        <td>Description</td>
        <td>Pic</td>
        <td>Company</td>
        <td>Scraped</td>
      </tr>
    headers
    data.each_with_index do |row, i|

      html << <<-row
        <tr id="row_#{i}">
          <td><img src="#{row[:picture]}" height="200" /></td>
          <td><a href="#{row[:link]}">#{row[:address]}</a></td>
          <td>#{row[:price]}</td>
          <td>#{row[:description]}</td>
          <td>#{row[:company]}</td>
          <td>
            <a href="#{row[:link]}">#{row[:created_at]}</a>
            <button class="btn" data-clipboard-target="#row_#{i}"><img src='https://clipboardjs.com/assets/images/clippy.svg' /></button>
          </td>
        </tr>
      row
    end
    html << "</table><script>new Clipboard('.btn');</script>"

    File.open("houses.html", "w") { |file| file.write(html) }
  end
end

scraper = PropertyScraper.new
scraper.run
scraper.output_html
