require_relative "base"

class PropertyScraper
  class Trademe < Base
    def farm
      get_page("https://www.trademe.co.nz/browse/categoryattributesearchresults.aspx?134=10&136=&153=&132=PROPERTY&49=0&49=300000&122=0&122=0&29=&123=0&123=0&search=1&sidebar=1&cid=5748&rptpath=350-5748-")
    end

    def get_page(url)
      data = []

      browser.goto(url)

      browser.lis(css: ".listingCard").each do |property|
        data << {
          price: property.div(css: ".list-view-card-price").text,
          description: property.links[1].text,
          link: property.links[1].href.split("?")[0],
          address: property.div(css: ".property-card-subtitle").text,
          picture: property.images.first.src,
          company: "Trademe"
        }
      end

      browser.close

      data
    end
  end
end