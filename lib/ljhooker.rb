require_relative "base"

class PropertyScraper
  class Ljhooker < Base
    def farm
      get_page("https://www.ljhooker.co.nz/search/property?tt=buy&pid=&r=Otago&d=&s%5B%5D=&ss=1&bp1=&bp2=200000&rp1=&rp2=&b1=&b2=&pt=&b=&c=&k=&searchType=residential&op=Find+Properties&form_build_id=form-m42ELxCvevWr_mXvWm0xzple9YJDEdaBXzdkTRJWK1M&form_id=residential_property_search_form")
    end

    def get_page(url)
      data = []

      browser.goto(url)

      browser.divs(css: ".property_snippet").each do |property|
        data << {
          price: property.h4.text,
          description: property.p.text,
          link: property.links.last.href,
          address: property.h3.text,
          picture: property.images.first.src,
          company: "LJHooker"
        }
      end

      browser.close

      data
    end
  end
end