require_relative "base"

class PropertyScraper
  class Raywhite < Base
    def farm
      get_page("http://raywhite.co.nz/Residential_Property")
    end

    def get_page(url)
      data = []

      browser.goto(url)

      browser.select_list(name: "region").select("Otago")
      browser.select_list(name: "toPriceForSale").select("$#{PropertyScraper::PRICE_MAX/1000}k")
      browser.input(type: "submit").click

      browser.divs(css: ".galleryViewBox").each do |property|
        data << {
          price: property.div(css: ".price").text,
          description: property.h3.text,
          link: property.links.first.href,
          address: property.p.text,
          picture: property.links.first.images.first.src,
          company: "Raywhite"
        }
      end

      browser.close

      data
    end
  end
end