require_relative "base"

class PropertyScraper
  class Cutlers < Base
    def farm
      get_page("http://cutlers.co.nz/listings/?keyword=&listing-category=for-sale&location=&listing-type=&bedrooms=&bathrooms=&parking=&min=&max=#{PropertyScraper::PRICE_MAX}&orderby=&order=")
    end

    def get_page(url)
      data = []

      browser.goto(url)

      browser.divs(css: ".listing").each do |property|
        data << {
          price: property.span(css: ".listing-price-value").text,
          description: "",
          link: property.links.first.href,
          address: property.h4.text,
          picture: property.images.first.src,
          company: "Cutlers"
        }
      end

      browser.close

      data
    end
  end
end