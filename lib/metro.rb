require_relative "base"

class PropertyScraper
  class Metro < Base
    def farm
      get_page("http://metrorealty.co.nz/search.php?add_district=18&add_city=69&add_suburb=&typ_type=1&typ_sub=&ser_pricemin=0&ser_pricemax=#{PropertyScraper::PRICE_MAX}&ser_bedroomsmin=0&ser_bedroomsmax=9999&search=1&order=recent")
    end

    def get_page(url)
      data = []

      browser.goto(url)

      browser.divs(css: ".listing").each do |property|
        data << {
          price: property.ps.last.text,
          description: property.ps.first.text,
          link: property.links.first.href,
          address: property.ps[-2].text.gsub("\n", ", "),
          picture: property.images.first.src,
          company: "Metro"
        }
      end

      browser.close

      data
    end
  end
end