require_relative "base"

class PropertyScraper
  class Livingcorporation < Base
    def farm
      get_page("http://www.livingcorporation.com/Buy?type=&minprice=0&maxprice=#{PropertyScraper::PRICE_MAX}&minroom=0&maxroom=0&ser_keyword=&search=1&searchtype=buy")
    end

    def get_page(url)
      data = []

      browser.goto(url)

      browser.divs(css: ".listing").each do |property|
        data << {
          price: "",
          description: property.p.text,
          link: property.links.first.href,
          address: property.links.first.text,
          picture: property.a(css: ".preview").style.match("http.+").to_s.split("\"").first,
          company: "Living Corporation"
        }
      end

      browser.close

      data
    end
  end
end