require_relative "base"

class PropertyScraper
  class Freedom < Base
    def farm
      get_page("http://www.freedomrealty.net.nz/property-search/?property-id=&location=any&type=any&status=any&bedrooms=any&bathrooms=any&min-price=any&max-price=#{PropertyScraper::PRICE_MAX}&min-area=&max-area=&orderby=date-desc")
    end

    def get_page(url)
      data = []

      browser.goto(url)

      browser.articles(css: ".property-item").each do |property|
        clean_price = property.h5.small.text
        data << {
          price: property.h5.text.gsub(clean_price, ""),
          description: property.p.text,
          link: property.links.first.href,
          address: property.links.first.text,
          picture: property.images.first.src,
          company: "Freedom"
        }
      end

      browser.close

      data
    end
  end
end