require "pry"
require "nokogiri"
require "watir"

class PropertyScraper
  class Base
    def initialize

    end

    def browser
      @browser ||= Watir::Browser.new :chrome, headless: true
    end
  end
end
