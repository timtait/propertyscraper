require_relative "base"

class PropertyScraper
  class Edinburgh < Base
    def farm
      get_page("https://www.edinburghrealty.co.nz/search-results?q=advanced&for=Sale&area=&price_from=&price_to=#{PropertyScraper::PRICE_MAX}&rent_price_from=&rent_price_to=&bedrooms=&bathrooms=")
    end

    def get_page(url)
      data = []

      browser.goto(url)
      browser.window.resize_to(100, 100) #addresses show up on a small screen
      sleep 10

      browser.divs(css: ".housetile").each do |property|
        data << {
          price: "",
          description: "",
          link: property.links.first.href,
          address: property.spans(css: ".housetile__address").first.text,
          picture: "https://www.edinburghrealty.co.nz/#{property.style.split("\"")[1]}",
          company: "Edinburgh"
        }
      end

      browser.close

      data
    end
  end
end