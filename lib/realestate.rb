require_relative "base"

class PropertyScraper
  class Realestate < Base
    def farm
      get_page("https://www.realestate.co.nz/residential/sale?q=&qai=&by=date_desc&cat=1&lct=r46&maxp=#{PropertyScraper::PRICE_MAX}&qo=0&ql=20")
    end

    def get_page(url)
      data = []

      browser.goto(url)

      browser.divs(css: ".tile-listing").each do |property|
        next if property.html.include?("google_ads_iframe")
        data << {
          price: property.h4.text,
          description: property.links.first.text,
          link: property.links.first.href,
          address: property.h5.text,
          picture: property.image(css: ".ember-view").src,
          company: "Realestate"
        }
      end

      browser.close

      data
    end
  end
end