require_relative "base"

class PropertyScraper
  class Nidd < Base
    def farm
      get_page("https://nidd.co.nz/listings?order=&direction=&view_type=grid-view&type=buy&address=&property_type=&price_from=&price_to=#{PropertyScraper::PRICE_MAX}&beds=&baths=&cars=")
    end

    def get_page(url)
      data = []

      browser.goto(url)

      browser.divs(css: ".property-item").each do |property|
        data << {
          price: property.div(css: ".price-tag").text.gsub("Price: ", ""),
          description: "",
          link: property.links.first.href,
          address: property.h3s.first.text,
          picture: property.images.first.src,
          company: "Nidd"
        }
      end

      browser.close

      data
    end
  end
end