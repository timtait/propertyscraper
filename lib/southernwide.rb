require_relative "base"

class PropertyScraper
  class Southernwide < Base
    def farm
      get_page("https://southernwiderealestate.co.nz/search/?result-display=list&quick=&action_find=Find&locationfirst=region-1&location=region-1&branch=&price-to=#{PropertyScraper::PRICE_MAX}#")
    end

    def get_page(url)
      data = []

      browser.goto(url)

      browser.divs(css: ".listing-result").each do |property|
        address = ""
        if property.li(css: ".icon-uilocation").exists?
          address = property.li(css: ".icon-uilocation").text.strip
        end

        data << {
          price: property.div(css: ".price").text,
          description: property.p.text,
          link: property.links.first.href,
          address: address,
          picture: property.images.map(&:src).detect{|x| x.include?("assets/listings")},
          company: "Southern Wide"
        }
      end

      browser.close

      data
    end
  end
end