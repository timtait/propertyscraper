require_relative "base"

class PropertyScraper
  class Pgg < Base
    def farm
      get_page("https://www.pggwre.co.nz/listings/region/Otago/citytown/all/maxprice/#{PropertyScraper::PRICE_MAX}/bedrooms/Min/bathrooms/Min/carparks/Min/property_section/Residential/view/grid/sort/most-recent/")
    end

    def get_page(url)
      data = []

      browser.goto(url)

      browser.divs(css: ".each_property").each do |property|
        data << {
          price: property.strong.text,
          description: property.span.text,
          link: property.links.first.href,
          address: property.h3.text,
          picture: property.images.first.src,
          company: "PGG"
        }
      end

      browser.close

      data
    end
  end
end