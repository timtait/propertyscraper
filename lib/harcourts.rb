require_relative "base"

class PropertyScraper
  class Harcourts < Base
    def farm
      get_page("http://harcourts.co.nz/Property/Residential?pageid=-1&search=&formsearch=true&OriginalTermText=&OriginalLocation=22010&location=22010&proptype=&min=&max=#{PropertyScraper::PRICE_MAX}&minbed=&maxbed=&view=list")
    end

    def get_page(url)
      data = []

      browser.goto(url)

      browser.divs(css: ".listingContent").each do |property|
        data << {
          price: property.div(css: ".propFeatures").h3.text,
          description: property.p.text,
          link: property.h2.links.first.href,
          address: property.div(css: ".listAddress").h3.text,
          picture: property.parent.div(css: ".listingImg").images.first.src,
          company: "Harcourts"
        }
      end

      browser.close

      data
    end
  end
end